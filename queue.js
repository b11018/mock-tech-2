let collection = [];

// Write the queue functions below.
const enqueue = (data) => {
	collection[collection.length] = data;
	return collection;
};

const dequeue = () => {
	for (let i = 1; i < collection.length; i++){ 
		collection[i - 1] = collection[i];
		collection.length--;
	// collection[0] = undefined;
	return collection;
	}
};

const print = () => {
	if(collection.length > 0){
		for (i = 0; i < collection.length - 1; i++){
			console.log(collection[i]);
		}
	}
	return collection;
};

const front = () => {
	return collection[0];
};

const size = () => {
	return collection.length;
};

const isEmpty = () => {
	if(collection.length !== 0){
		return false;
	}
	return true;
};

module.exports = {
	collection,
	enqueue,
	dequeue,
	print,
	front,
	size,
	isEmpty
};